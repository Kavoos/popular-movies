package com.android.kavoos.popularmovies;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.kavoos.popularmovies.model.Movie;
import com.squareup.picasso.Picasso;

public class DetailActivityFragment extends Fragment {

    private Movie movie;

    public DetailActivityFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        Intent intent = getActivity().getIntent();

        if (intent != null && intent.hasExtra(Intent.EXTRA_REFERRER)) {
            movie = (Movie) intent.getSerializableExtra(Intent.EXTRA_REFERRER);

            if (!movie.getOriginalTitle().equals(movie.getTitle())) {
                ((TextView) rootView.findViewById(R.id.detail_textview_original_title))
                        .setText(movie.getTitle() + "\n(" + movie.getOriginalTitle() + ")");
            } else {
                ((TextView) rootView.findViewById(R.id.detail_textview_original_title))
                        .setText(movie.getOriginalTitle());
            }
            ((TextView) rootView.findViewById(R.id.detail_textview_release_date)).setText(movie.getReleaseDate().substring(0, 4));
            ((TextView) rootView.findViewById(R.id.detail_textview_vote_average)).setText(movie.getVoteAverage() + "/10");
            ((TextView) rootView.findViewById(R.id.detail_textview_overview)).setText(movie.getOverview());

            ImageView imageView = (ImageView) rootView.findViewById(R.id.detail_imageview_thumbnail);

            String posterPath = "http://image.tmdb.org/t/p/w185" + movie.getPosterPath();
            Picasso.with(getContext()).load(posterPath).into(imageView);
        }

        return rootView;
    }
}
