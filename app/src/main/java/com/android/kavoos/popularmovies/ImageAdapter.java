package com.android.kavoos.popularmovies;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.android.kavoos.popularmovies.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends ArrayAdapter<Movie> {

    private ArrayList<Movie> movies;

    public ImageAdapter(Context context, int resource, int imageViewResourceId, List<Movie> objects) {
        super(context, resource, imageViewResourceId, objects);
        this.movies = (ArrayList<Movie>) objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        gridView = inflater.inflate(R.layout.grid_item_movie, null);

        ImageView imageView = (ImageView) gridView.findViewById(R.id.grid_item_movie_imageview);

        String posterPath =  "http://image.tmdb.org/t/p/w342" + movies.get(position).getPosterPath();
        Picasso.with(getContext()).load(posterPath).into(imageView);

        return gridView;
    }
}
