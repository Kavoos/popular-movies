package com.android.kavoos.popularmovies;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.android.kavoos.popularmovies.model.Movie;
import com.android.kavoos.popularmovies.model.TVShow;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivityFragment extends Fragment {

    private ImageAdapter imageAdapter;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_main, container, false);

        imageAdapter = new ImageAdapter(
                getActivity(),
                R.layout.grid_item_movie,
                R.id.grid_item_movie_imageview,
                new ArrayList<Movie>());

        GridView gridView = (GridView) rootview.findViewById(R.id.gridview_movie);
        gridView.setAdapter(imageAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Movie movie = imageAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), DetailActivity.class)
                        .putExtra(Intent.EXTRA_REFERRER, movie);
                startActivity(intent);
            }
        });

        return rootview;
    }

    private void updateMovie() {
        FetchMovieTask movieTask = new FetchMovieTask();
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String sortType = sharedPrefs.getString(
                getString(R.string.pref_sort_key),
                getString(R.string.pref_sort_most_popular));
        String listType = sharedPrefs.getString(
                getString(R.string.pref_list_key),
                getString(R.string.pref_list_movie));

        String sort = "";
        String list = "";

        switch (sortType) {
            case "top_rated": sort = "Top Rated";
                break;
            case "popular": sort = "Popular";
                break;
        }

        switch (listType) {
            case "movie": list = "Movies";
                break;
            case "tv": list = "TV Shows";
                break;
        }

        getActivity().setTitle(sort + " " + list);

        movieTask.execute(listType, sortType);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateMovie();
    }

    public class FetchMovieTask extends AsyncTask<String, Void, Movie[]> {

        @Override
        protected Movie[] doInBackground(String... params) {

            if (params.length == 0) {
                return null;
            }

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            String movieJsonStr = null;

            try {
                final String MOVIE_BASE_URL = "http://api.themoviedb.org/3/";
                final String APPID_PARAM = "api_key";

                Uri builtUri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                        .appendPath(params[0])
                        .appendPath(params[1])
                        .appendQueryParameter(APPID_PARAM, BuildConfig.TMDB_API_KEY)
                        .build();

                URL url = new URL(builtUri.toString());

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }
                movieJsonStr = buffer.toString();
            } catch (IOException e) {
                Log.e("MainActivityFragment", "Error ", e);
                return null;
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("MainActivityFragment", "Error closing stream", e);
                    }
                }
            }

            try {
                return getMovieDataFromJson(movieJsonStr, params[0]);
            } catch (JSONException e) {
                Log.e("MainActivityFragment", e.getMessage(), e);
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Movie[] result) {
            if (result != null) {
                imageAdapter.clear();
                for (Movie movie : result) {
                    imageAdapter.add(movie);
                }
            }
        }
    }

    private Movie[] getMovieDataFromJson(String movieJsonStr, String listType) throws JSONException {

        //For both Movie and TV show JSON files
        final String TMDB_RESULTS = "results";
        final String TMDB_POSTER_PATH = "poster_path";
        final String TMDB_OVERVIEW = "overview";
        final String TMDB_VOTE_AVERAGE = "vote_average";
        //Only for Movie JSON file
        final String TMDB_ORIGINAL_TITLE = "original_title";
        final String TMDB_TITLE = "title";
        final String TMDB_RELEASE_DATE = "release_date";
        //Only for TV Shows JSON file
        final String TMDB_ORIGINAL_NAME = "original_name";
        final String TMDB_NAME = "name";
        final String TMDB_FIRST_AIR_DATE = "first_air_date";

        JSONObject jsonObject = new JSONObject(movieJsonStr);
        JSONArray results = jsonObject.getJSONArray(TMDB_RESULTS);

        Movie[] resultMovies = new Movie[results.length()];

        for(int i = 0; i < results.length(); i++) {

            JSONObject movie = results.getJSONObject(i);

            String posterPath = movie.getString(TMDB_POSTER_PATH);
            String overview = movie.getString(TMDB_OVERVIEW);
            String voteAverage = movie.getString(TMDB_VOTE_AVERAGE);

            if (listType.equals("movie")) {
                String originalTitle = movie.getString(TMDB_ORIGINAL_TITLE);
                String title = movie.getString(TMDB_TITLE);
                String releaseDate = movie.getString(TMDB_RELEASE_DATE);

                resultMovies[i] = new Movie(originalTitle, title, posterPath, overview, voteAverage, releaseDate);
            } else if (listType.equals("tv")) {
                String originalName = movie.getString(TMDB_ORIGINAL_NAME);
                String name = movie.getString(TMDB_NAME);
                String firstAirDate = movie.getString(TMDB_FIRST_AIR_DATE);

                resultMovies[i] = new TVShow(originalName, name, posterPath, overview, voteAverage, firstAirDate);
            }
        }

        return resultMovies;
    }
}
