package com.android.kavoos.popularmovies.model;

import java.io.Serializable;

/**
 * Created by Kavoos on 17-Jan-17.
 */

public class TVShow extends Movie implements Serializable {

    public TVShow(String originalName, String name, String posterPath, String overview, String voteAverage, String firstAirDate) {
        super(originalName, name, posterPath, overview, voteAverage, firstAirDate);
    }

}
